#ifndef BOARD_H
#define BOARD_H

#include <QFile>
#include <iostream>
#include <vector>
#include <QXmlStreamReader>
#include <QDebug>
#include <QString>
#include <QObject>
#include <filesystem>
#include <unistd.h>
#include <string>

#include "logger.h"
using std::filesystem::current_path;

class Board : public QObject
{
    Q_OBJECT

public:
    explicit Board(QObject *parent = nullptr);
    static std::vector<QString> loadBoard();
    static void writeBoard(std::vector<QString> board); // predavam pole, abych ho mohla projit a kazdy prvek zapsat

signals:

};

#endif // BOARD_H
