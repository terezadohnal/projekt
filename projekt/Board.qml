import QtQuick 2.0


Grid{
    property int tileCount: 5
    property alias tileSpacing: board.spacing
    x: tileSpacing
    y: tileSpacing
    id: board
    columns: tileCount
    spacing: 5
    anchors.centerIn: parent

    Repeater{
        model: tileCount*tileCount

        Tile{
            tileIndex: index
            width: (board.width/tileCount)-(board/(tileCount-1)*tileCount)
            height: (board.height/tileCount)-(board/(tileCount-1)*tileCount)
        }
    }
}
