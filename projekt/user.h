#ifndef USER_H
#define USER_H

#include <iostream>
#include <QObject>
#include "Attributes.h"
#include "QJsonDocument"
#include "QJsonObject"
#include "QJsonArray"
#include "QFile"
#include "QXmlStreamReader"
#include "logger.h"

class User : public QObject
{
    Q_OBJECT
private:
    int m_coins;
    int m_level;
    Attributes m_attributes;
public:
    explicit User(QObject *parent = nullptr);
    void loadAttributes();
    void writeAtributes();

    void setCoins(int coins);
    void setLevel(int level);
    void setAttributes(int wood, int bricks,float concrete);
    int getLevel();
    int getCoins();
    Attributes getUserAttributes();
    void print();
signals:

};

#endif // USER_H
