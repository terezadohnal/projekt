import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15

Window {
    width: 1100
    height: 650
    visible: true
    title: qsTr("Patamaluky")
    maximumHeight: height
    maximumWidth: width
    minimumHeight: height
    minimumWidth: width

    Popup {
      id: popupMain
      x: 350
      y: 250
      //anchors.centerIn: parent
      width: 400
      height: 150
      contentItem: Text {
          id: popupText
          text: qsTr("Vitajte v našej slovenskej hre (po anglicky)\n. Celá hrá sa ovláda myšou.\n Pri určitom leveli si môžete kupovať rôzne budovy.\n Vďaka levelu vám za danú\n časovú periódu pribúda pasívny príjem.\n Čím vyšší level, tým vyšší pasívny príjem.\n ")
      }
      closePolicy: popupMain.CloseOnEscape

      Button {
          padding: 220
          text: "X"
          anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 120 }
          onClicked: popupMain.visible = false
          anchors.bottom: popupMain.Bottom
          }
  }


    Item {
        property int coinsHouse: 20
        property int coinsPost: 30
        property int coinsShop: 40
    }


    Image {
        id: background
        source: "images/background.png"
        width: 1100
        height: 650

    }

    TopMenu {
        id: menuOnTop
    }

    Popup {
       id: buyHouse
       x: 25
       y: 25
       padding: 10
       //anchors.centerIn: parent
       width: 250
       height: 60
       opacity: 0
       enter: Transition {
               NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
           }
       contentItem: Text {
           id: buypopupText
           text: qsTr("You've bought house for $100")

       }
     closePolicy: buyHouse.CloseOnEscape
     Button {
       padding: 20
       text: "X"
       anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 20 }
       onClicked: buyHouse.visible = false
       }
     }

    Timer {
        interval: 500
        running: true
        repeat: false
        onTriggered:{

            popupMain.open()
        }
    }

    Popup {
       id: buyPost
       x: 25
       y: 25
       padding: 10
       //anchors.centerIn: parent
       width: 250
       height: 60
       opacity: 0
       enter: Transition {
               NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
           }
       contentItem: Text {

           text: qsTr("You've bought post for $200")

       }
     closePolicy: buyPost.CloseOnEscape
     Button {
       padding: 20
       text: "X"
       anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 20 }
       onClicked: buyPost.visible = false
       }
     }

    Popup {
       id: buyKauf
       x: 25
       y: 25
       padding: 10
       //anchors.centerIn: parent
       width: 250
       height: 60
       opacity: 0
       enter: Transition {
               NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
           }
       contentItem: Text {

           text: qsTr("You've bought Kaufland for $300")

       }
     closePolicy: buyKauf.CloseOnEscape
     Button {
       padding: 20
       text: "X"
       anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 20 }
       onClicked: buyKauf.visible = false
       }
     }


    Rectangle {
        id: backgroundBoard
        x: 300
        y: 75
        width: 500
        height: 500
        color: "darkgreen"
        anchors.centerIn: parent
        radius: 4

        Board {
            height: parent.height-12
            width: (height + 2)
            anchors.centerIn: parent
            tileSpacing: 5
            tileCount: 5

        }
    }
}

