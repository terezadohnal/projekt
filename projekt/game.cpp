#include "game.h"

Game::Game(QObject *parent)
    : QObject{parent}
{
    user = new User();
    m_board = Board::loadBoard();
    m_typeBuilding = "a";
}

QString Game::getCharacter(int index) {
    return m_board.at(index);
}

void Game::saveResults(int index, QString type){
    m_board.at(index) = type;
    Board::writeBoard(getBoard());
    user->writeAtributes();
}

void Game::setMoney(int money){
    user->setCoins(money);
}


int Game::getMoney(){
    return user->getCoins();
}

void Game::decreaseMoney(int money){
    setMoney(user->getCoins() - money);
    emit moneyChanged();
}

void Game::increaseMoney(int money){
    setMoney(user->getCoins() + money);
    emit moneyChanged();
}

std::vector<QString> Game::getBoard(){
    return m_board;
}

void Game::setState(bool state){
    m_state = state;
}

void Game::setTypeBuilding(QString building){
    m_typeBuilding = building;
}

bool Game::getState(){
    return m_state;
}

QString Game::getTypeBuilding(){
    return m_typeBuilding;
}

void Game::setLevel(int level) {
    user->setLevel(level);
}

int Game::getLevel() {
    return user->getLevel();
}

void Game::changeLevel(){
    if(getMoney() >= 1000) {
        setLevel(2);
        emit checkLevel();
    }
    if (getMoney() >= 2000) {
        setLevel(3);
        emit checkLevel();
    }
    if(getMoney() < 1000){
        setLevel(1);
        emit checkLevel();
    }

}

