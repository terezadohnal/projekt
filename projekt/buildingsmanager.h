#ifndef BUILDINGSMANAGER_H
#define BUILDINGSMANAGER_H
#include <QXmlStreamReader>
#include <QObject>
#include "building.h"
#include <vector>
#include <iostream>
#include <QFile>
#include <QDebug>


class buildingsManager : public QObject
{
    Q_OBJECT
private:
   std::vector<Building> typeOfBuildings;
public:
    explicit buildingsManager(QObject *parent = nullptr);
    Q_INVOKABLE Building getBuildingFromType(std::string type);
    Q_INVOKABLE std::vector<Building> getBuildingsToBuy(int level); 
    Q_INVOKABLE std::vector<Building> loadingBuildings();
signals:

};

#endif // BUILDINGSMANAGER_H
