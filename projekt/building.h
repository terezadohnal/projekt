#ifndef BUILDING_H
#define BUILDING_H
#include <iostream>

struct Building {
    int price;
    int level;
    std::string type;
    int returns;
};

#endif // BUILDING_H
