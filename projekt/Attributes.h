#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H

struct Attributes {
    int wood;
    int bricks;
    float concrete;
};

#endif // ATTRIBUTES_H
