#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

class Logger{
    static Logger* s_instance;
    vector<string> m_errors;
public:
    static Logger* getLogger();
    void addError(string error);
    void storeErrors();

private:
    Logger();
    void loadErrors();
};

#endif // LOGGER_H
