#ifndef GAME_H
#define GAME_H
#include <QString>
#include <QObject>
#include <vector>

#include "board.h"
#include "user.h"
#include "logger.h"

class Game : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int money READ getMoney NOTIFY moneyChanged)
    Q_PROPERTY(int level READ getLevel NOTIFY checkLevel)
private:
    std::vector<QString> m_board;
    User* user;
    bool m_state;
    QString m_typeBuilding;

    void setMoney(int money);
    void setLevel(int level);

public:
    explicit Game(QObject *parent = nullptr);
    Q_INVOKABLE QString getCharacter(int index);
    Q_INVOKABLE void saveResults(int index, QString type);
    Q_INVOKABLE int getMoney();
    Q_INVOKABLE int getLevel();
    Q_INVOKABLE void setTypeBuilding(QString building);
    Q_INVOKABLE QString getTypeBuilding();


    std::vector<QString> getBoard();
    void setState(bool state);
    bool getState();

signals:
    void moneyChanged();
    void checkLevel();
public slots:
    void decreaseMoney(int money);
    void increaseMoney(int money);
    void changeLevel();
};

#endif // STATICGAME_H
