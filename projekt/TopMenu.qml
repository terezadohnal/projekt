import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.15

Item {

    states: [
        State {
            name: "PostAvailability"
            PropertyChanges {
                target: buyPost
                enabled: false
            }
        }
    ]

    id: theTop
    Timer{
        id:updateValue
        interval: 10000
        repeat:true
        running: true
        triggeredOnStart: true
        onTriggered:
                   if (game.getLevel() > 2) {
                             game.increaseMoney(150);
                         } else if (game.getLevel() > 1){
                             game.increaseMoney(100)
                         } else if (game.getLevel() > 0){
                                     game.increaseMoney(50)
                   }
      }

    Timer{
        id:updateButtons
        interval: 1000
        repeat:true
        running: true
        triggeredOnStart: true
        onTriggered:
                   if (game.getLevel() > 2) {
                       buyKaufland.enabled = true;
                       buyPost.enabled = true;
                        } else if (game.getLevel() > 1){
                       buyPost.enabled = true;
                         } else if (game.getLevel() > 0){
                   } /*else {
                       buyKaufland.enabled = false;
                       buyPost.enabled = false;
                   }*/
      }


    Button {
        id: deleteBuilding
        x: 710
        y: 600
        padding: 20
        text: "Delete building"
        onClicked: {
            popup.open()
            game.setTypeBuilding("del");
        }

    }

    Button {
        id: buyHouse
        x: 580
        y: 600
        padding: 20
        enabled: true
        text: "Buy a new house"
        onClicked: {

            theTop.state = "postAvailability"

            popup.open()
            game.setTypeBuilding("d");
        }

    }



    Button {

        id: buyPost
        x: 425
        y: 600
        padding: 20
        text: "Buy a new post office"
        enabled:false
        onClicked: {

            popup.open()
            game.setTypeBuilding("p");
        }

    }

    Button {
        id: buyKaufland
        x: 300
        y: 600
        padding: 20
        text: "Buy a new shop"
        enabled:false
        onClicked: {

            popup.open()
            game.setTypeBuilding("k");
        }

    }

    Rectangle {
        y: 20
        x: 380
        width: 195
        height: 45
        color: "lightblue"
        radius: 8
        border.width: 3
        border.color: "black"
        Text {
            id: textMoney
            anchors.centerIn: parent;
            font.bold: true
            font.pixelSize: 24
            text: "Money: " + game.money
        }
    }

    Rectangle {
        y: 20
        x: 580
        width: 165
        height: 45
        color: "lightblue"
        radius: 8
        border.width: 3
        border.color: "black"
        Text {

            anchors.centerIn: parent;
            font.bold: true
            font.pixelSize: 24
            text: "Level: " + game.level
        }
    }

    Popup {
      id: popup
      x: 100
      y: 300
      //anchors.centerIn: parent
      width: 100
      height: 50
      contentItem: Text {
          id: popupText
          text: qsTr("Select the tile")
      }
      closePolicy: popup.CloseOnEscape

      Button {
          padding: 20
          text: "X"
          anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 20 }
          onClicked: popup.visible = false
          anchors.left: popup.ight
          }
  }

//    PopupMoney{
//        id:moneyPopup
//        x:300
//        y:300
//        width: 300
//        height: 200
//        Image {
//            id: houseMoney
//            source: "images"
//        }

//    }
}
