#include "board.h"

Board::Board(QObject *parent)
    : QObject{parent}
{

}

std::vector<QString> Board::loadBoard(){
    std::vector<QString> board(25, "n");

    QFile file(":/board.xml");
    if (file.open(QIODevice::ReadOnly)){
        // napojit XML parser
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();
        // hlavni smycka dokud neni konec dokumentu
        while(!xmlReader.isEndDocument()){
            // zkontroluji element, ktery prisel
            if (xmlReader.isStartElement()){
                QString name = xmlReader.name().toString();
                if (name=="field"){
                    int index = xmlReader.attributes().value("index").toInt();
                    QString boardCount = xmlReader.readElementText();
                    board.at(index) = boardCount;
                } else if (name == "board"){
                    // skip
                } else {
                    Logger* chyba = Logger::getLogger();
                    chyba->addError("Narazil jsem na jiny element nez field");
                    chyba->storeErrors();
                }
            }
            // nactu dalsi element
            xmlReader.readNext();
        }
    } else {
        Logger* chyba = Logger::getLogger();
        chyba->addError("Soubor se nepovedlo otevrit");
        chyba->storeErrors();
    }
    return board;

}

void Board::writeBoard(std::vector<QString> board){ // predam pole
    //QString fileName = "file://C:/Users/georg/Desktop/programming/projekt/projekt/board.xml"; //cesta Juraj
    //QFile file(fileName.split("//").at(1)); //Juraj
    //    vyuzit metodu pro vraceni cesty

    QString fileName = "/Users/tereza/School/qt/projekt/projekt/board.xml"; // specifikovani cesty MAC
    QFile file(fileName);  //MAC
    if(file.open(QIODevice::WriteOnly)){
        QXmlStreamWriter writer;
        writer.setDevice(&file);
        writer.setAutoFormatting(true);
        writer.writeStartDocument(); //nic
        writer.writeStartElement("board"); // board

        //pro kazdy prvek provedu tohle
        for(int i = 0; i < board.size(); i++){
            writer.writeStartElement("field"); // field
            writer.writeAttribute("index", QString::number(i)); //index, cislo
            writer.writeCharacters(board.at(i)); //znak nachazejici se na danem miste
            writer.writeEndElement(); // /field
        }
        writer.writeEndElement(); // /board
        writer.writeEndDocument();
        file.close();
    } else{
        Logger* chyba = Logger::getLogger();
        chyba->addError("Soubor se nepovedlo otevrit");
        chyba->storeErrors();
    }

}

