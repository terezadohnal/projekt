import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.15

Rectangle {
    property alias tileIndex: label.text
    //property string previousState: ""
    id: tile
    width: 94
    height: 94
    border.width: 3
    border.color: "lightblue"
    color: "lightblue"
    radius: 8



    states: [
        State {
            name: "hovered"
            PropertyChanges {
                target: tile
                border.color: "white"
            }
        },
        State {
            name: "house"
            PropertyChanges {
                target: homeImg
                opacity: 100
            }
//            PropertyChanges {
//                target: buyHouse
//                opacity: 0
//            }
            PropertyChanges {
                target: label
                visible: false

            }
//            PropertyChanges {
//                target: menuOnTop.

//                opacity: 0
//            }
        },

        State {
            name: "post"
            PropertyChanges {
                target: postImg
                opacity: 100
            }
//            PropertyChanges {
//                target: buyPost
//                enabled: true
//            }
            PropertyChanges {
                target: label
                visible: false

            }
        },

        State {
            name: "postAvailable"
//            PropertyChanges {
//                target: buyPost
//                opacity: 100
//                enabled: true
//            }
        },

        State {
            name: "kauf"
            PropertyChanges {
                target: kaufImg
                opacity: 100
            }
            PropertyChanges {
                target: buyKauf
                opacity: 100
            }
            PropertyChanges {
                target: label
                visible: false

            }
        },
        State {
            name: "not enough money"
            PropertyChanges {
                target: label
                opacity: 100
            }
            PropertyChanges {
                target: homeImg
                opacity: 0
            }
            PropertyChanges {
                target: tile
                border.color: "red"
            }
        }
    ]


    Image {
        id: homeImg
        opacity: 0
        source: "images/domcek.png"
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        //anchors.fill: parent
    }

    Image {
        id: postImg
        opacity: 0
        source: "images/posta.png"
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        //anchors.fill: parent
    }

    Image {
        id: kaufImg
        opacity: 0
        source: "images/obchod.png"
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        //anchors.fill: parent
    }



    MouseArea{
        anchors.fill: parent
        hoverEnabled: true

        Timer {
            interval: 500
            running: true
            repeat: false
            onTriggered:{


                if (game.getCharacter(tileIndex) === 'h') {
                  tile.state = "house"

                } else if (game.getCharacter(tileIndex) === 'p') {
                    tile.state = "post"
                } else if (game.getCharacter(tileIndex) === 'k') {
                        tile.state = "kauf"
                }
            }
        }

        onClicked: {
            var tileCharacter = game.getCharacter(tileIndex);
            var money = game.getMoney();
            var building = game.getTypeBuilding();
            var level = game.getLevel();

            if (money >= 100 && tileCharacter == "n" && building == "d" && level >= 1) {
                tile.state = "house"
                game.decreaseMoney(100);
                game.saveResults(tileIndex, "h");
                buyHouse.open()
                game.setTypeBuilding("a");
            } else if (money >= 200 && tileCharacter == "n" && building == "p" && level >= 2){
                tile.state = "post"
                game.decreaseMoney(200);
                game.saveResults(tileIndex, "p");
                buyPost.open()
                game.setTypeBuilding("a");
            } else if (money >= 300 && tileCharacter == "n" && building == "k" && level >= 3){
                tile.state = "kauf"
                game.decreaseMoney(300);
                game.saveResults(tileIndex, "k");
                buyKauf.open()
                game.setTypeBuilding("a");
            } else if (building == "del" && tileCharacter != "n") {
//                label.text = "$"
                tile.state = "not enough money"
                game.saveResults(tileIndex, "n");
                game.setTypeBuilding("a");
                game.increaseMoney(100);
            }
            else if (tileCharacter == "n"){
                label.text = "$"
                tile.state = "not enough money"
            }

            game.changeLevel();


        onEntered: {
            //tile.state = "hovered"
            //tile.previousState = tile.state
        }

        onExited: {
            //tile.state = tile.previousState
            tile.border.color = "lightblue"
        }
    }


    Text {
        id: label
        text: "#"
        font {
            pixelSize: 24
            bold: true
        }
        color: "lightblue"
        anchors.centerIn: parent
   }



    }
}
