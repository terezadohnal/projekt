#include "building.h"

building::building(QObject *parent, unsigned int price, unsigned int level , QString type)
    : QObject{parent}
{
    this->price = price;
    this->level = level;
    this->type = type;
}


void building::setPosition(unsigned int row, unsigned int col){
    this->position.row = row;
    this->position.col = col;
}

Position building::getPosition(){
    return this->position;
}

unsigned int building::getPrice(){
    return this->price;
}

void building::loadInfo(){

}

void building::writeInfo(){

}
