#include "buildingsmanager.h"


buildingsManager::buildingsManager(QObject *parent)
    : QObject{parent}
{

}

Building buildingsManager::getBuildingFromType(std::string type){
    for(unsigned int i = 0; i < typeOfBuildings.size(); ++i) {
        if(typeOfBuildings.at(i).type == type){
            return typeOfBuildings.at(i);
        }
    }
}

std::vector<Building> buildingsManager::getBuildingsToBuy(int level){
   std::vector<Building> buildingsToBuy;
   for(unsigned int i = 0; i < typeOfBuildings.size(); ++i) {
       if(typeOfBuildings.at(i).level == level){
          buildingsToBuy.push_back(typeOfBuildings.at(i));
       }
   }
   return buildingsToBuy;
}

std::vector<Building> buildingsManager::loadingBuildings(){
    std::vector<Building> buildingsType(5,{0,0,"n",0});

    QFile file(":/building.xml");
    if (file.open(QIODevice::ReadOnly)){
        // napojit XML parser
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();
        // hlavni smycka dokud neni konec dokumentu
        while(!xmlReader.isEndDocument()){
//            if (xmlReader.isStartElement()){
//                QString name = xmlReader.name().toString();
//                if (name=="field"){
//                    int index = xmlReader.attributes().value("index").toInt();
//                    QString boardCount = xmlReader.readElementText();
//                    board.at(index) = boardCount;
//                } else if (name == "board"){
//                    // skip
//                } else {
//                    qCritical() << "Narazil jsem na element jiny nez field" << name <<"\n";
//                }
//            }
            // nactu dalsi element
            xmlReader.readNext();
        }
       } else {
        qCritical() << "Soubor se nepovedlo otevrit\n";
    }

    return buildingsType;
}
