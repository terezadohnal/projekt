#include "user.h"

User::User(QObject *parent)
    : QObject{parent}
{
    loadAttributes();
}

void User::loadAttributes(){
//    QFile file("C:/Users/Marketa/Desktop/user.xml");
//    QFile file("/Users/tereza/School/qt/projekt/projekt/user.xml");
    int wood, bricks;
    float conc;

    QFile file(":/users.xml");
    if(file.open(QIODevice::ReadOnly)){
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();

       while(!xmlReader.isEndDocument()){
           if (xmlReader.isStartElement()){
               QString name = xmlReader.name().toString();
               if(name == "coins"){
                   int coins = xmlReader.readElementText().toInt();
                   setCoins(coins);
               } else if (name == "level"){
                   int level =xmlReader.readElementText().toInt();
                   setLevel(level);
               }else if (name == "wood"){
                   wood = xmlReader.readElementText().toInt();
               }else if (name == "bricks"){
                   bricks = xmlReader.readElementText().toInt();
               }else if (name == "concrete"){
                   conc = xmlReader.readElementText().toFloat();
               }else if (name == "attributes"){
               //skip
               }else {
                   Logger* chyba = Logger::getLogger();
                   chyba->addError("Narazil jsem na jiny element");
                   chyba->storeErrors();
               }
               setAttributes(wood, bricks, conc);
        }
           xmlReader.readNext();
       }
    }else {
        Logger* chyba = Logger::getLogger();
        chyba->addError("Soubor se nepovedlo otevrit");
        chyba->storeErrors();
    }
}

void User::writeAtributes(){
//    QString fileName = "C:/Users/Marketa/Desktop/user.xml"; // specifikovani cesty
    QString fileName = "/Users/tereza/School/qt/projekt/projekt/users.xml";
    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly)){
        QXmlStreamWriter writer;
        writer.setDevice(&file);
        writer.setAutoFormatting(true);

        writer.writeStartDocument();
        writer.writeStartElement("attributes"); // board

        //pro kazdy prvek provedu tohle
        writer.writeStartElement("coins");
        writer.writeCharacters(QString::number(m_coins));
        writer.writeEndElement();
        writer.writeStartElement("level");
        writer.writeCharacters(QString::number(m_level));
        writer.writeEndElement();
        writer.writeStartElement("wood");
        writer.writeCharacters(QString::number(m_attributes.wood));
        writer.writeEndElement();
        writer.writeStartElement("bricks");
        writer.writeCharacters(QString::number(m_attributes.bricks));;
        writer.writeEndElement();
        writer.writeStartElement("concrete");
        writer.writeCharacters(QString::number(m_attributes.concrete));
        writer.writeEndElement();

        writer.writeEndElement();
        writer.writeEndDocument();
        file.close();
    } else{
        Logger* chyba = Logger::getLogger();
        chyba->addError("Soubor se nepovedlo otevrit");
        chyba->storeErrors();
    }
}

void User::setCoins(int coins){
    m_coins = coins;
}

void User::setLevel(int level){
    m_level = level;
}

void User::setAttributes(int wood, int bricks,float concrete){
    m_attributes.wood = wood;
    m_attributes.bricks = bricks;
    m_attributes.concrete = concrete;
}

int User::getLevel(){
    return m_level;
}

int User::getCoins(){
    return m_coins;
}

Attributes User::getUserAttributes(){
    return m_attributes;
}

void User::print(){
    std::cout << "penize: " << getCoins() << std::endl;
    std::cout << "level: " << getLevel() << std::endl;
    std::cout << "drevo: " << getUserAttributes().wood << std::endl;
    std::cout << "cihly: " << getUserAttributes().bricks << std::endl;
    std::cout << "beton: " << getUserAttributes().concrete << std::endl;
}
